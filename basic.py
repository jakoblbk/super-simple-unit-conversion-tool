def getKey(list,value):
    return dict((v, k) for k, v in list.items())[value]


while True:
    #e
    print('\n')
    print('------- Unit Conversion Tool -------\n')
    print('For help type "help".')
    operationWanted = input('\n' + 'Do you want to view the list of possible unit conversions? \ny/n ')
    operationWantedYes = '\nPossible units to choose from:\n[m] [dm] [cm] [mm] [miles] [yards] [leagues] [inches] [foots]'
    print('\n')
    if operationWanted == 'y':
        print(operationWantedYes)
    elif operationWanted == 'n':
        print('\n')
    else :
        print('------- Help -------\n')
        print('This is a tool for converting several different units.\nEnter the unit without [].\nDo NOT put a space after the unit.\nThe requested conversion is accurate to about eight decimals, the general one to about four.\n')
        print('Written by: Jakob Linnebank\nVersion: 0.01\nLicence: MIT\nThis software was created for educational purposes.\nSchifferstadt, Germany 2018\n')
        print('------- Help -------\n')
        operationWanted = input('Do you want to view the list of possible unit conversions? \ny/n ')
        if operationWanted == 'y':
            print(operationWantedYes)
        if operationWanted == 'n':
            print('\n')


    unitFrom = input('\nConvert from: ')
    unitTo = input('Convert to: ')
    valueInput = float(input('Value: '))

    factors = {"km":0.001,"m":1,"dm":10,"cm":100,"mm":1000,"miles":(1/1609.344),"yards":1.094,"leagues":(1/5556),"inches":39.37,"foots":3.281}
    factor = factors[unitFrom]
    secondFactor = factors[unitTo]
    #v



    calculating = valueInput / factor
    result = calculating * secondFactor

    # requested conversion

    generalFactors = {}
    for key in factors:
        newKey = "general" + key
        generalFactors[newKey] = factors[key] * calculating


#a

    print('\n')
    print('',valueInput, unitFrom, 'are', round(result,8), unitTo,'.\n')
    genConversionChoice = input('Do you want to view the general conversion as well? y/n ')
    if genConversionChoice == 'y':
        print('')
        print('', 'General Conversion\n')
        for element in generalFactors:
        #    unit = getKey(factors,generalFactors[element])
            print('',valueInput, unitFrom, 'are', round(generalFactors[element], 4))
    quitCMD = input('Do you want to quit?\ny/n ')
    if quitCMD == 'y':
        break
