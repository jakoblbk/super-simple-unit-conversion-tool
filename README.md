# super simple unit conversion tool

This program is written for educational purposes and aims to show some of the possibilities Python offers with nothing but very (very) basic code. It does **not** aim to teach and/or achieve the best possible pythan syntax (or even "good" one). 

There are two files:

* very-basic.py
* basic.py

They differ in "complexity" but serve the same purpose / have the same functionalities.