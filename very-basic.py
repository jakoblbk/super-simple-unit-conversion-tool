while True:
    #e
    print('\n')
    print('------- Unit Conversion Tool -------\n')
    print('For help type "help".')
    operationWanted = input('\n' + 'Do you want to view the list of possible unit conversions? \ny/n ')
    operationWantedYes = '\nPossible units to choose from:\n[m] [dm] [cm] [mm] [miles] [yards] [leagues] [inches] [foots]' 
    print('\n')
    if operationWanted == 'y':
        print(operationWantedYes)
    if operationWanted == 'n':
        print('\n')
    if operationWanted == 'help':
        print('------- Help -------\n')
        print('This is a tool for converting several different units.\nEnter the unit without [].\nDo NOT put a space after the unit.\nThe requested conversion is accurate to about eight decimals, the general one to about four.\n')
        print('Written by: Jakob Linnebank\nVersion: 0.01\nLicence: MIT\nThis software was created for educational purposes.\nSchifferstadt, Germany 2018\n')
        print('------- Help -------\n')
        operationWanted = input('Do you want to view the list of possible unit conversions? \ny/n ')
        if operationWanted == 'y':
            print(operationWantedYes)
        if operationWanted == 'n':
            print('\n')    
    

    unitFrom = input('\nConvert from: ')
    unitTo = input('Convert to: ')
    valueInput = float(input('Value: '))

    #v

    if unitFrom == 'm':
        factor = 1
    if unitFrom == 'dm':
        factor = 10
    if unitFrom == 'cm':
        factor = 100
    if unitFrom == 'mm':
        factor = 1000
    if unitFrom == 'miles':
        factor = (1 / 1609.344)
    if unitFrom == 'yards':
        factor = 1.094
    if unitFrom == 'leagues':
        factor = (1 / 5556)
    if unitFrom == 'inches':
        factor = 39.37
    if unitFrom == 'foots':
        factor = 3.281

    # conversion from inputFrom to metres


    if unitTo == 'm':
        secondFactor = 1
    if unitTo == 'dm':
        secondFactor = 10
    if unitTo == 'cm':
        secondFactor = 100
    if unitTo == 'mm':
        secondFactor = 1000
    if unitTo == 'miles':
        secondFactor = (1 / 1609.344)
    if unitTo == 'yards':
        secondFactor = 1.094
    if unitTo == 'leagues':
        secondFactor = (1 / 5556)
    if unitTo == 'inches':
        secondFactor = 39.37
    if unitTo == 'foots':
        secondFactor = 3.281

    genM = 1
    genDM = 10
    genCM = 100
    genMM = 1000
    genMiles = (1 / 1609.344)
    genYards = 1.094
    genLeagues = (1 / 5556)
    genInches = 39.37
    genFoots = 3.281

    calculating = valueInput / factor
    result = calculating * secondFactor

    # requested conversion

    generalM = calculating * genM
    generalDM = calculating * genDM
    generalCM = calculating * genCM
    generalMM = calculating * genMM
    generalMiles = calculating * genMiles
    generalYards = calculating * genYards
    generalLeagues = calculating * genLeagues
    generalInches = calculating * genInches
    generalFoots = calculating * genFoots

    # general conversion
#a

    print('\n')
    print('',valueInput, unitFrom, 'are', format(result, '.8f'), unitTo,'.\n')
    genConversionChoice = input('Do you want to view the general conversion as well? y/n ')
    if genConversionChoice == 'y':
        print('')
        print('', 'General Conversion\n')
        print('',valueInput, unitFrom, 'are', format(generalM, '.4f'),'Metres\n',valueInput, unitFrom, 'are', format(generalDM, '.4f'),'Decimetres\n',valueInput, unitFrom, 'are', format(generalCM, '.4f'),'Centimetres\n',valueInput, unitFrom, 'are', format(generalMM, '.4f'),'Millimetres\n',valueInput, unitFrom, 'are', format(generalMiles, '.4f'),'Miles\n',valueInput, unitFrom, 'are', format(generalYards, '.4f'),'Yards\n',valueInput, unitFrom, 'are', format(generalLeagues, '.4f'),'Leagues\n',valueInput, unitFrom, 'are', format(generalInches, '.4f'),'Inches\n',valueInput, unitFrom, 'are', format(generalFoots, '.4f'),'Foots\n')
    print('')
    quitCMD = input('Do you want to quit?\ny/n ')
    if quitCMD == 'y':
        break
